{
  "bboxMode": 0,
  "collisionKind": 2,
  "separateMasks": false,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 1,
  "bbox_right": 62,
  "bbox_top": 3,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"98fc0256-0b0b-4752-91e6-226a05c04211","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"LayerId":{"name":"ff771916-b252-470d-8e91-65abc40d4e17","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"98fc0256-0b0b-4752-91e6-226a05c04211","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"LayerId":{"name":"ff771916-b252-470d-8e91-65abc40d4e17","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"resourceVersion":"1.0","name":null,"tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Roid_L","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"resourceVersion":"1.0","name":"98fc0256-0b0b-4752-91e6-226a05c04211","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e4a9072e-18f8-4b9e-af95-96a635461d5d","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"LayerId":{"name":"ff771916-b252-470d-8e91-65abc40d4e17","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e4a9072e-18f8-4b9e-af95-96a635461d5d","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"LayerId":{"name":"ff771916-b252-470d-8e91-65abc40d4e17","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"resourceVersion":"1.0","name":null,"tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Roid_L","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"resourceVersion":"1.0","name":"e4a9072e-18f8-4b9e-af95-96a635461d5d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4dd51524-c9ca-4e52-8566-72624d957d00","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"LayerId":{"name":"ff771916-b252-470d-8e91-65abc40d4e17","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4dd51524-c9ca-4e52-8566-72624d957d00","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"LayerId":{"name":"ff771916-b252-470d-8e91-65abc40d4e17","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"resourceVersion":"1.0","name":null,"tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Roid_L","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"resourceVersion":"1.0","name":"4dd51524-c9ca-4e52-8566-72624d957d00","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4b2fed35-5c90-4f4a-97f4-76e3be882c45","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"LayerId":{"name":"ff771916-b252-470d-8e91-65abc40d4e17","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4b2fed35-5c90-4f4a-97f4-76e3be882c45","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"LayerId":{"name":"ff771916-b252-470d-8e91-65abc40d4e17","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"resourceVersion":"1.0","name":null,"tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Roid_L","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"resourceVersion":"1.0","name":"4b2fed35-5c90-4f4a-97f4-76e3be882c45","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bd8698cb-b82b-48fb-b3b6-627f4d6fa652","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"LayerId":{"name":"ff771916-b252-470d-8e91-65abc40d4e17","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bd8698cb-b82b-48fb-b3b6-627f4d6fa652","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"LayerId":{"name":"ff771916-b252-470d-8e91-65abc40d4e17","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"resourceVersion":"1.0","name":null,"tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Roid_L","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"resourceVersion":"1.0","name":"bd8698cb-b82b-48fb-b3b6-627f4d6fa652","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Roid_L","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 0.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 5.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"da4ddd48-46e9-4807-9efa-084094fdd4ce","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"98fc0256-0b0b-4752-91e6-226a05c04211","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b5b8b633-8441-4052-a552-f25538994fb3","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e4a9072e-18f8-4b9e-af95-96a635461d5d","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7d94e5c0-bb1b-4de1-b58f-ab715328240c","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4dd51524-c9ca-4e52-8566-72624d957d00","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a4090650-f1b3-46b0-aaf8-1c9487b2dc77","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4b2fed35-5c90-4f4a-97f4-76e3be882c45","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"66957542-42ff-42d5-bcce-95944eb7837d","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bd8698cb-b82b-48fb-b3b6-627f4d6fa652","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 32,
    "yorigin": 32,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Roid_L","path":"sprites/spr_Roid_L/spr_Roid_L.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"ff771916-b252-470d-8e91-65abc40d4e17","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Sprites",
    "path": "folders/Roids/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Roid_L",
  "tags": [],
  "resourceType": "GMSprite",
}