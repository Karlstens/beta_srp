/// @desc Keyboard Controls

//Left and Right
if keyboard_check(vk_left) or keyboard_check(ord("A"))
{
phy_angular_velocity-= 10;
}

if keyboard_check(vk_right) or keyboard_check(ord("D")) 
{
phy_angular_velocity+= 10;
}

//Reverse Thust
if keyboard_check(vk_down) or keyboard_check(ord("S")) 
	{
	phy_angular_damping += 0.1 //stablise ship upon de-acceleration
	physics_apply_local_impulse (0,0, -0.25, 0);
	}
	else
	{
		//need to think of a better control/dampening mechanism
		phy_angular_damping = 0.1;  //resets dampening to ship default
	}
//Thrust
if keyboard_check(vk_up) or keyboard_check(ord("W")) 
	{ 
	phy_angular_damping += 0.1 //stablise ship upon acceleration
	physics_apply_local_impulse (0, 0, 0.25, 0);

	var line_length = 5
	if thruster_engage
	    { 
		thruster_engage = false;
        alarm[1] = irandom_range(2,7);
		with (instance_create_layer((phy_position_x+lengthdir_x(line_length,phy_rotation+180)),(phy_position_y+lengthdir_y(line_length,phy_rotation)),"Layer_Roids",obj_Thrust))
			{
			   	phy_speed_x = other.phy_speed_x
				phy_speed_y = other.phy_speed_y
				phy_rotation = other.phy_rotation-irandom_range(170,190)
				physics_apply_local_impulse(0, 0, 0.001,0 );
			} 
		}
	}
	
/// @desc Initial Booster
if keyboard_check_pressed(vk_up) or keyboard_check_pressed(ord("W")) 
	{
		var line_length = 15
		physics_apply_local_impulse (0, 0, 10, 0);
		repeat(5)
			{
			with (instance_create_layer((phy_position_x+lengthdir_x(line_length,phy_rotation+180)),(phy_position_y+lengthdir_y(line_length,phy_rotation)),"Layer_Roids",obj_Thrust))
				{
				   	phy_speed_x = other.phy_speed_x;
					phy_speed_y = other.phy_speed_y;
					phy_rotation = other.phy_rotation-irandom_range(170,190);
					physics_apply_local_impulse(0, 0, 0.001,0 );
			} 
	}
}

if keyboard_check_pressed(vk_space)  /// put into script plz
{
	scr_vector_push();
}