/// @desc Mouse Buttons

if device_mouse_check_button_pressed(0, mb_left)
{
	var line_length = 20;
	physics_apply_local_impulse(0, 0,-0.5 ,0 ); //recoil
	with (instance_create_layer((phy_position_x-lengthdir_x(line_length,phy_rotation+180)),(phy_position_y-lengthdir_y(line_length,phy_rotation)),"Layer_Roids",obj_bullet))
	    {
	   	phy_speed_x = other.phy_speed_x;
		phy_speed_y = other.phy_speed_y;
		phy_rotation = other.phy_rotation-90
		physics_apply_local_impulse(0, 0,0 ,1 );
		}	
}

if device_mouse_check_button_pressed(0, mb_right)
{

	old_angle = image_angle;
	spin_dir = phy_rotation;
}

if device_mouse_check_button(0,mb_right)
{
	if spin_dir > phy_rotation
	{
	phy_angular_velocity-= 10;
	}
	else
	{
	phy_angular_velocity+= 10;
	}
	

	if abs(old_angle -phy_rotation ) > 37
		{
		var line_length = -28
		with (instance_create_layer((phy_position_x-lengthdir_x(line_length,phy_rotation+180)),(phy_position_y-lengthdir_y(line_length,phy_rotation)),"Layer_Roids",obj_bullet))
		    {
		   	phy_speed_x = other.phy_speed_x;
			phy_speed_y = other.phy_speed_y;
			physics_apply_local_impulse(0, 0,0 ,0.01 );
		
			}	
			old_angle = phy_rotation;	
		}
		
}