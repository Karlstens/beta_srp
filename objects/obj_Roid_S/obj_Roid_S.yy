{
  "spriteId": {
    "name": "spr_Roid_S",
    "path": "sprites/spr_Roid_S/spr_Roid_S.yy",
  },
  "solid": false,
  "visible": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": {
    "name": "obj_Roid_Master",
    "path": "objects/obj_Roid_Master/obj_Roid_Master.yy",
  },
  "physicsObject": true,
  "physicsSensor": false,
  "physicsShape": 0,
  "physicsGroup": 0,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.5,
  "physicsLinearDamping": 0.0,
  "physicsAngularDamping": 0.0,
  "physicsFriction": 0.0,
  "physicsStartAwake": true,
  "physicsKinematic": true,
  "physicsShapePoints": [
    {"x":8.0,"y":8.0,},
    {"x":8.0,"y":8.0,},
  ],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":8,"collisionObjectId":null,"parent":{"name":"obj_Roid_S","path":"objects/obj_Roid_S/obj_Roid_S.yy",},"resourceVersion":"1.0","name":null,"tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [
    {"varType":0,"value":"1.0","rangeEnabled":false,"rangeMin":0.0,"rangeMax":10.0,"listItems":[],"multiselect":false,"filters":[],"resourceVersion":"1.0","name":"Impact_Speed","tags":[],"resourceType":"GMObjectProperty",},
  ],
  "overriddenProperties": [],
  "parent": {
    "name": "Obj",
    "path": "folders/Roids/Obj.yy",
  },
  "resourceVersion": "1.0",
  "name": "obj_Roid_S",
  "tags": [],
  "resourceType": "GMObject",
}