/// @desc Small Roid hitting "Medium" roid

//This code doesn't take into account the direction of travel, 
//thus will incorrectly destroy to colliding objects traveling in the same direction.

//if (abs(phy_angular_velocity - other.phy_angular_velocity) > 30) //cheap hack to check direction diff
//Two roids playing chasey will not smash at extreme speeds.
//Smash_Speed = sqrt(abs((phy_speed_x - other.phy_speed_x)^2) + abs((phy_speed_y - other.phy_speed_y)^2))

//Smash_Speed = sqrt(power(phy_speed_x - other.phy_speed_x,2) + power(phy_speed_y - other.phy_speed_y,2))

/// @desc small roid impact

//low impact

scr_create_debris(2,obj_Debris,0);
var Impact_Speed = 1;
var Smash_Speed = sqrt(abs(power(phy_speed_x - other.phy_speed_x,2)) + abs(power(phy_speed_y - other.phy_speed_y,2)));;

//high impact

if Smash_Speed > Impact_Speed
{
	hitpoints += other.damage;
	if (hitpoints >= maxhealth)
		{
		scr_place_roid(choose(3,4),obj_Roid_S);
		instance_destroy(self);
		}
	image_index = hitpoints;	
	with(other) //small roid
	{
		hitpoints += other.damage
		if (hitpoints >= maxhealth)
		{
			scr_create_debris(10,obj_Debris,0);	
			instance_destroy();
		}
		image_index = hitpoints;
	}
}