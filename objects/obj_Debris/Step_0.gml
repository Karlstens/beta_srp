/// @description Fade Debris

image_alpha -= fade_rate * ceil(instance_number(obj_Debris)/2);

if (image_alpha <= 0)
	{
		instance_destroy();
	}