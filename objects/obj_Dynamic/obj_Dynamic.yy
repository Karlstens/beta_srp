{
  "spriteId": null,
  "solid": false,
  "visible": false,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": null,
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 0,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 100.0,
  "physicsAngularDamping": 0.0,
  "physicsFriction": 0.0,
  "physicsStartAwake": false,
  "physicsKinematic": false,
  "physicsShapePoints": [
    {"x":0.0,"y":0.0,},
    {"x":32.0,"y":0.0,},
    {"x":32.0,"y":32.0,},
    {"x":0.0,"y":32.0,},
  ],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":4,"collisionObjectId":{"name":"obj_Dynamic","path":"objects/obj_Dynamic/obj_Dynamic.yy",},"parent":{"name":"obj_Dynamic","path":"objects/obj_Dynamic/obj_Dynamic.yy",},"resourceVersion":"1.0","name":null,"tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":2,"eventType":3,"collisionObjectId":null,"parent":{"name":"obj_Dynamic","path":"objects/obj_Dynamic/obj_Dynamic.yy",},"resourceVersion":"1.0","name":null,"tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "Game Config",
    "path": "folders/Game Config.yy",
  },
  "resourceVersion": "1.0",
  "name": "obj_Dynamic",
  "tags": [],
  "resourceType": "GMObject",
}