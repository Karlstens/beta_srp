

//new function - almost working, but density/distance/magnitude needs some work. Currently runs a negative number giving the wrong direction/speed to far away objects
function scr_vector_push_() 
{
	target[0] = obj_Roid_Master;
	target[1] = obj_Debris;
	target[2] = obj_Thrust;
	target[3] = obj_bullet;
		
var count = array_length(target); 
	
repeat (count) {
		count--;
		
		with(target[count])
		{
			var phy_rot_cache =  phy_rotation;
			phy_rotation = point_direction(obj_Player.x, obj_Player.y,phy_position_x, phy_position_y )*-1;
			physics_apply_local_force(0,0,1000-(4*point_distance(obj_Player.phy_position_x,obj_Player.phy_position_y,phy_position_x,phy_position_y)),0);
			phy_rotation = phy_rot_cache;
		
		}
	}
}



//old function - almost can delete.
function scr_vector_push() {

	var yy = 0; with(obj_Roid_Master)
	{
			var phy_rot_cache =  phy_rotation;
			phy_rotation = point_direction(obj_Player.x, obj_Player.y,phy_position_x, phy_position_y )*-1;
			physics_apply_local_force(0,0,1000-point_distance(obj_Player.phy_position_x,obj_Player.phy_position_y,phy_position_x,phy_position_y),0);
			phy_rotation = phy_rot_cache;
		yy++;
	}

	var yy = 0; with(obj_Debris)
	{
			var phy_rot_cache =  phy_rotation;
			phy_rotation = point_direction(obj_Player.x, obj_Player.y,phy_position_x, phy_position_y )*-1;
			physics_apply_local_force(0,0,0.5,0);
			phy_rotation = phy_rot_cache;
		yy++;
	}

	var yy = 0; with(obj_Thrust)
	{
			var phy_rot_cache =  phy_rotation;
			phy_rotation = point_direction(obj_Player.x, obj_Player.y,phy_position_x, phy_position_y )*-1;
			physics_apply_local_force(0,0,0.1,0);
			phy_rotation = phy_rot_cache;
		yy++;
	}

	var yy = 0; with(obj_bullet)
	{
			var phy_rot_cache =  phy_rotation;
			phy_rotation = point_direction(obj_Player.x, obj_Player.y,phy_position_x, phy_position_y )*-1;
			physics_apply_local_force(0,0,100,0);
			phy_rotation = phy_rot_cache;
		yy++;
	}
}
